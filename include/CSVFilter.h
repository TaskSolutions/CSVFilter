//
//  CSVFilter.hpp
//
//  Created by Saud Ahmed on 7/25/17.
//
//

#ifndef CSVFilter_hpp
#define CSVFilter_hpp

#include <stdio.h>
#include <iostream>
#include "Macros.h"
#include <map>
#include <vector>

using namespace std;

enum DelimiterType {
    kNone,
    kComma,
    kSpace
};

class CSVFilter
{
protected:
    CC_SYNTHESIZE(int **, array2D_, Array2D);
    
private:
    CSVFilter();
    virtual ~CSVFilter();
    
    // Static variables.
    static CSVFilter* instance;
    
    //Instance variables.
    int totalBadValues_;
    int numberOfRows_;
    int numberOfColumns_;
    
    
    std::vector<std::map<std::string, int> > badValuesArray_;
    
    DelimiterType delimiterType_;
    
    //Instance Methods
    void determineDelimiterInsideCSVFile(string fileName);
    void calculateDimensionsOfCSVFile(string fileName);
    void allocateMemoryTo2DArray();
    void parseCSVFile(string fileName);
    void storeRowsAndColumnsForBadValues();
    void interpolateBadValuesFromNearestNeighbor();
    void storeResultToCSVFile(string fileName);
    void deallocateArrays();
    
public:
    
    // Static methods.
    static CSVFilter* getInstance();
    
    //Instance Methods
    void filterBadValuesFromCSVFile(const string inputFileName, const string outputFileName);
    
};
#endif /* CSVFilter_hpp */
