//
//  Helper.h
//
//  Created by Saud Ahmed on 7/25/17.
//
//

#ifndef Helper_h
#define Helper_h

#include <iostream>

inline bool checkSubStringExistInStringInLowerCase(std::string subStr, std::string checkStr)
{
    bool exists;
    
    std::transform(subStr.begin(), subStr.end(), subStr.begin(), ::tolower);
    std::transform(checkStr.begin(), checkStr.end(), checkStr.begin(), ::tolower);
    
    exists = checkStr.find(subStr.c_str()) != std::string::npos;
    
    return exists;
}

#endif /* Helper_h */
