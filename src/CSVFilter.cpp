//
//  CSVFilter.cpp
//
//  Created by Saud Ahmed on 7/25/17.
//
//

#include "CSVFilter.h"

#include <fstream>
#include <sstream>
#include <math.h>
#include "Helper.h"

/***** Static variables *****/

CSVFilter* CSVFilter::instance = nullptr;

/***** End static variables *****/

/***** Constructors and destructor. *****/

CSVFilter::CSVFilter():totalBadValues_(0),
numberOfRows_(0),
numberOfColumns_(0),delimiterType_(kNone){
    this->array2D_ = nullptr;
}

CSVFilter::~CSVFilter(){
    
}

/***** End constructors and destructor. *****/

/***** Static methods. *****/

CSVFilter* CSVFilter::getInstance(){
    if (CSVFilter::instance == nullptr) {
        CSVFilter::instance = new CSVFilter();
    }
    
    return CSVFilter::instance;
}

/***** End static methods. *****/

/***** Instance methods. *****/

void CSVFilter::filterBadValuesFromCSVFile(const string inputFileName, const string outputFileName){
    this->determineDelimiterInsideCSVFile(inputFileName);
    this->calculateDimensionsOfCSVFile(inputFileName);
    this->allocateMemoryTo2DArray();
    this->parseCSVFile(inputFileName);
    this->storeRowsAndColumnsForBadValues();
    this->interpolateBadValuesFromNearestNeighbor();
    this->storeResultToCSVFile(outputFileName);
    
    this->deallocateArrays();
}

void CSVFilter::determineDelimiterInsideCSVFile(string fileName){
    std::ifstream csvFile (fileName.c_str(), std::ifstream::in);
    
    std::string readline;
    std::getline(csvFile, readline);
    
    if (checkSubStringExistInStringInLowerCase(",", readline)){
        this->delimiterType_ = kComma;
    }
    else if(checkSubStringExistInStringInLowerCase(" ", readline)){
        this->delimiterType_ = kSpace;
    }
    else{
        this->delimiterType_ = kNone;
    }
}

void CSVFilter::calculateDimensionsOfCSVFile(string fileName){
    std::ifstream csvFile (fileName.c_str(), std::ifstream::in);
    
    while (1){
        
        std::string readline;
        std::getline(csvFile, readline);
        
        std::stringstream ss(readline);
        
        std::string value;
        std::getline(ss, value, ',');
        if ( ss.eof() )
        {
            break;
        }
        
        this->numberOfColumns_++;
        
        if (csvFile.eof())
        {
            break;
        }
        
        this->numberOfRows_++;
    }
}

void CSVFilter::allocateMemoryTo2DArray(){
    this->array2D_ = new int*[this->numberOfRows_];
    for(int index = 0; index < this->numberOfRows_; index++) {
        this->array2D_[index] = new int[this->numberOfColumns_];
    }
}

void CSVFilter::parseCSVFile(string fileName){
    
    std::ifstream csvFile (fileName.c_str(), std::ifstream::in);
    
    for(int row = 0; row < this->numberOfRows_; ++row)
    {
        std::string line;
        std::getline(csvFile, line);
        
        std::stringstream iss(line);
        
        for (int col = 0; col < this->numberOfColumns_; ++col)
        {
            std::string val;
            std::getline(iss, val, ',');
            
            std::stringstream convertor(val);
            convertor >> this->array2D_[row][col];
        }
    }
}

void CSVFilter::storeRowsAndColumnsForBadValues(){
    for(int row = 0; row < this->numberOfRows_; ++row)
    {
        for (int col = 0; col < this->numberOfColumns_; ++col)
        {
            int value = this->array2D_[row][col];
            
            if (value == 0)
            {
                std::string rowString = std::to_string(row);
                std::string columnString = std::to_string(col);
                
                std::map<string,int> object;
                object["row"] = row;
                object["col"] = col;
                
                this->badValuesArray_.push_back(object);
            }
        }
    }
}

void CSVFilter::interpolateBadValuesFromNearestNeighbor(){
    //Interpolate bad values
    for(std::vector<int>::size_type index = 0; index != this->badValuesArray_.size(); index++) {
        std::map<string,int> object = this->badValuesArray_[index];
        
        int row = object["row"];
        int col = object["col"];
        
        float interpolatedValue = 0;
        
        if (row == 0 && col == 0)
        {
            //Top Left
            int neighbor1 = this->array2D_[row][col + 1];
            int neighbor2 = this->array2D_[row + 1][col + 1];
            int neighbor3 = this->array2D_[row + 1][col];
            
            interpolatedValue = (float(neighbor1 + neighbor2 + neighbor3)) / 3;
        }
        else if ((row == this->numberOfRows_ - 1) && col == 0)
        {
            //Bottom Left
            int neighbor1 = this->array2D_[row - 1][col];
            int neighbor2 = this->array2D_[row - 1][col + 1];
            int neighbor3 = this->array2D_[row][col + 1];
            
            interpolatedValue = (float(neighbor1 + neighbor2 + neighbor3)) / 3;
        }
        else if (row == 0 && (col == this->numberOfColumns_ - 1))
        {
            //Top Right
            int neighbor1 = this->array2D_[row][col - 1];
            int neighbor2 = this->array2D_[row + 1][col - 1];
            int neighbor3 = this->array2D_[row + 1][col];
            
            interpolatedValue = (float(neighbor1 + neighbor2 + neighbor3)) / 3;
        }
        else if ((row == this->numberOfRows_ - 1) && (col == this->numberOfColumns_ - 1))
        {
            //Bottom Right
            int neighbor1 = this->array2D_[row - 1][col];
            int neighbor2 = this->array2D_[row - 1][col - 1];
            int neighbor3 = this->array2D_[row][col - 1];
            
            interpolatedValue = (float(neighbor1 + neighbor2 + neighbor3)) / 3;
        }
        else if (row == 0 && (col > 0 && col < (this->numberOfColumns_ - 1)))
        {
            //Top Between
            int neighbor1 = this->array2D_[row][col - 1];
            int neighbor2 = this->array2D_[row + 1][col - 1];
            int neighbor3 = this->array2D_[row + 1][col];
            int neighbor4 = this->array2D_[row + 1][col + 1];
            int neighbor5 = this->array2D_[row][col + 1];
            
            interpolatedValue = (float(neighbor1 + neighbor2 + neighbor3 + neighbor4 + neighbor5)) / 5;
        }
        else if ((row > 0 && row < (this->numberOfRows_ - 1)) && (col == (this->numberOfColumns_ - 1)))
        {
            //Right Between
            int neighbor1 = this->array2D_[row - 1][col];
            int neighbor2 = this->array2D_[row - 1][col - 1];
            int neighbor3 = this->array2D_[row][col - 1];
            int neighbor4 = this->array2D_[row + 1][col - 1];
            int neighbor5 = this->array2D_[row + 1][col];
            
            interpolatedValue = (float(neighbor1 + neighbor2 + neighbor3 + neighbor4 + neighbor5)) / 5;
        }
        else if (row == (this->numberOfRows_ - 1) && (col > 0 && col < (this->numberOfColumns_ - 1)))
        {
            //Bottom Between
            int neighbor1 = this->array2D_[row][col - 1];
            int neighbor2 = this->array2D_[row - 1][col - 1];
            int neighbor3 = this->array2D_[row - 1][col];
            int neighbor4 = this->array2D_[row - 1][col + 1];
            int neighbor5 = this->array2D_[row][col + 1];
            
            interpolatedValue = (float(neighbor1 + neighbor2 + neighbor3 + neighbor4 + neighbor5)) / 5;
        }
        else if ((row > 0 && row < (this->numberOfRows_ - 1)) && col == 0)
        {
            //Left Between
            int neighbor1 = this->array2D_[row - 1][col];
            int neighbor2 = this->array2D_[row - 1][col + 1];
            int neighbor3 = this->array2D_[row][col + 1];
            int neighbor4 = this->array2D_[row + 1][col + 1];
            int neighbor5 = this->array2D_[row + 1][col];
            
            interpolatedValue = (float(neighbor1 + neighbor2 + neighbor3 + neighbor4 + neighbor5)) / 5;
        }
        else if ((row > 0 && row < (this->numberOfRows_ - 1)) && (col > 0 && col < (this->numberOfColumns_ - 1)))
        {
            //In Between with 8 neighbors
            int neighbor1 = this->array2D_[row - 1][col - 1];
            int neighbor2 = this->array2D_[row - 1][col];
            int neighbor3 = this->array2D_[row - 1][col + 1];
            int neighbor4 = this->array2D_[row][col + 1];
            int neighbor5 = this->array2D_[row + 1][col + 1];
            int neighbor6 = this->array2D_[row + 1][col];
            int neighbor7 = this->array2D_[row + 1][col - 1];
            int neighbor8 = this->array2D_[row][col - 1];
            
            interpolatedValue = (float(neighbor1 + neighbor2 + neighbor3 + neighbor4 + neighbor5 + neighbor6 + neighbor7 + neighbor8)) / 8;
        }
        
        int value = ceil(interpolatedValue);
        
        this->array2D_[row][col] = value;
    }
}

void CSVFilter::storeResultToCSVFile(string fileName){
    //Store to an output CSV file
    std::ofstream ofs (fileName.c_str(), std::ofstream::out);
    
    for (int x = 0; x < this->numberOfRows_; x++)
    {
        std::string content = "";
        for (int y = 0; y < this->numberOfColumns_; y++)
        {
            if (y == this->numberOfColumns_ - 1)
            {
                content = content + std::to_string(this->array2D_[x][y]) + "\n";
            }
            else
            {
                if (this->delimiterType_ == kComma)
                {
                    content = content + std::to_string(this->array2D_[x][y]) + ",";
                }
                else if (this->delimiterType_ == kSpace)
                {
                    content = content + std::to_string(this->array2D_[x][y]) + " ";
                }
            }
        }
        ofs << content;
    }
    
    ofs.close();
}

void CSVFilter::deallocateArrays()
{
    //Deallocate memory
    for(int i = 0; i < this->numberOfRows_; ++i) {
        CC_SAFE_DELETE_ARRAY(this->array2D_[i]);
    }
    
    CC_SAFE_DELETE_ARRAY(this->array2D_);
}
/***** End Instance methods. *****/
