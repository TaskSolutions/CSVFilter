#include<iostream>
#include <fstream>

#include "CSVFilter.h"

int main(int argc, char *argv[]){

    if( argc == 1 )
    {
        printf("Enter input.csv and output.csv as arguments\n");
        
        return 0;
    }
    else if (argc == 2)
    {
        printf("Missing 2nd argument output.csv\n");
        
        return 0;
    }
    
    std::ifstream file("../resources/" + string(argv[1]));
    
    if (file.good())
    {
        CSVFilter::getInstance()->filterBadValuesFromCSVFile("../resources/" + string(argv[1]), "../resources/" + string(argv[2]));
    }
    else
    {
        printf("File %s does not exist", argv[1]);
        return 0;
    }
    
    
    return 0;
}
